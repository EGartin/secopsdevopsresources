# RSA University
### IR Threat Hunting Workshop
#### Resources List

## Links

### RSA NW Links
- [Rig Exploit kit writeup](https://community.rsa.com/community/products/netwitness/blog/2017/02/01/rig-ek-chronology-of-an-exploit-kit)
- [RSA Netwitness Hunting Guide](https://community.rsa.com/docs/DOC-62341)
- [RSA Netwitness Investigator (think free miniature logs and packets)](https://community.rsa.com/thread/188114)
- [RSA Netwitness Training](https://community.rsa.com/community/training/netwitness)

### Other Links
- [Good Look at Packers](http://resources.infosecinstitute.com/deep-dive-into-a-custom-malware-packer/)
- [Learn to Disassemble with Dr. Fu](http://fumalwareanalysis.blogspot.com/p/malware-analysis-tutorials-reverse.html)
- [Warning: Live Malware!](https://www.mediafire.com/folder/b8xxm22zrrqm4/BADINFECT)
- [DevOps explained by Amazon](https://aws.amazon.com/devops/)
- [DevOps Periodic Table of Elements](https://xebialabs.com/periodic-table-of-devops-tools/)

### Dashboard Links
- [Norse Honeypot Attack Visualization](http://map.norsecorp.com/)
- [Exploit Kit Tracker Visualization](http://ektracker.com/) 

## Tools
- [Process monitor](https://technet.microsoft.com/en-us/sysinternals/processmonitor.aspx)
- [Process Explorer](https://technet.microsoft.com/en-us/sysinternals/processexplorer.aspx)
- [Process hacker](http://processhacker.sourceforge.net/)
- [CFF Explorer](http://www.ntcore.com/exsuite.php)
- [Malzilla](http://malzilla.sourceforge.net/)
- [Ida Pro](https://www.hex-rays.com/index.shtml)
- [Binary Ninja](https://binary.ninja/)
- [ImmunityDebug](http://www.immunityinc.com/products/debugger/)
- [ILSpy](http://ilspy.net/)
- [Cuckoo Sandbox](http://www.cuckoosandbox.org/)
  Cuckoo is relatively easy to set up, highly extensible and capable of automating a great deal of common analysis. Out of the box, its pretty functional, but with some of the addons here, it is made far more potent.
- [VMCloak](http://vmcloak.org/)
  Automatically disguises most common indicators of virtualization for things like cuckoo.
- [RAT Builders](https://www.dropbox.com/s/lwci5t3ccjg9hpl/RATS.zip?dl=0)

**Clearance Warning:**

- [Tradecraft Do's and Dont's, IE what malware should look like](https://wikileaks.com/ciav7p1/cms/page_14587113.html)
- [In-memory Code Execution Specifications](https://wikileaks.com/ciav7p1/cms/files/ICE-Spec-v3-final-SECRET.pdf)

## Book Recommendations
- [Practical malware analysis](https://www.amazon.com/Practical-Malware-Analysis-Hands-Dissecting/dp/1593272901)
  A must read for anyone looking to get into the field. This book is laid out in a set of sequential and well designed labs to help take you from the basics all the way through many more advanced techniques. 

- [The IDA Pro Book](https://www.amazon.com/IDA-Pro-Book-2nd-ebook/dp/B005EI84TM)
  Once you complete, or even as you get to the middle of practical malware analysis, you will quickly find that you need to have a better understanding of decompilers, intermediary languages and Ida Pro. Ida pro is the central tool used in just about every single malware analysis exercise and it is very, very complex.Without the manual, you will frequently find yourself looking for answers that are hard to find online, but easy to find in the book.

- [The Art of Memory Forensics](https://www.amazon.com/Art-Memory-Forensics-Detecting-Malware/dp/1118825098)
  This book deals with how windows lays out memory and how the different operating system mechanisms like segmentation and ASLR/DEP work. It also gets into the specific hardware implementation details of the intel instruction set in relation to memory. The first four chapters of this book alone make it a necessity to pick up.

- [Violent python](https://www.amazon.com/Violent-Python-Cookbook-Penetration-Engineers-ebook/dp/B00ABY67JS)
  You will learn to use the python language to build something reminiscent of some of the malware you may have seen previously. It works through the design and building concepts of code that must execute in the restrained fashion malware does while teaching you advanced python concepts at the same time.

- [Low-Level Programming](https://www.amazon.com/Low-Level-Programming-Assembly-Execution-Architecture-ebook/dp/B073GN6V7P)
  Now, on to my current favorite programming book. Personally, I am a big fan of C/C++ since they allow me to make robust and powerful programs with little hassle or overhead. I had been working with c++ for a while prior to picking this book up and it really took my skills to a new level of efficacy. The book will have you build out your own assembly level programming libraries and then build your own programs with them. This really takes programming from the lowest level of abstraction to actual meaningful use of code.

- [Open Source Intelligence Techniques](https://www.amazon.com/Open-Source-Intelligence-Techniques-Information/dp/1530508908/)
  Open source intelligence techniques will teach you how to use the internet effectively to gather information on any target, human or technical. The book is essentially a set of recipes for how to search for and find information using free and open source resources. In addition, it features many very interesting case studies and stories by its author, a former police detective. This was written by the guy who put together a lot of what you saw in my database demo.

- [Art of Deception](https://www.amazon.com/Art-Deception-Controlling-Element-Security/dp/076454280X)
- [Art of Intrustion](https://www.amazon.com/Art-Intrusion-Exploits-Intruders-Deceivers/dp/0471782661)
  The Art of Deception and the Art of Intrusion are both written by Kevin Mitnick, one of the most infamous hackers of the 90s. His big claim to fame wasn’t that he could write the best exploits or build amazing malware, but rather that he could use an innate mastery of the social dynamic common to most people and turn that into getting whatever he wanted. These books, containing both stories from Kevin himself, and from other infamous hackers, provide a lot of potential material on how a breach is most easily accomplished by targeting the weakest element of security, the person behind the chair.

- [Cryptography Engineering](https://www.amazon.com/Cryptography-Engineering-Principles-Practical-Applications/dp/0470474246) 
  A quality deep dive into the applied use of cryptography and gets into things like custom base64 libraries, common encryption schemes and their uses and many other things. Cryptography is one of those subjects that requires a bit of knowledge to work with effectively and this is one of the few easy and accessible sources on the topic.

- [The Phoenix Project](https://www.amazon.com/Phoenix-Project-DevOps-Helping-Business-ebook/dp/B00AZRBLHO)
  A first hand account of a company in turmoil and in need of a drastic IT overhaul.  The book is a great conceptual introduction to the world of DevOps and a good example of how taking a step back can lead to a giant leap forward.  I recommend this to everyone who is unfamiliar with DevOps as it provides a better overview of what DevOps truly is, not just a list of tools that help enable the methodology.

If you have any good recommendations for tools, please feel free to share!

## LinkedIn
- [Elijah Gartin](https://www.linkedin.com/in/egartin)
- [Mike Zeiger](https://www.linkedin.com/in/mike-zeiger-184474b/)

